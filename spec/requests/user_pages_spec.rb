require 'spec_helper'

describe "UserPages" do
  describe "signup page" do
    before { visit signup_path+"?locale=fr" }

    it { response.should have_selector('h1',    content: "Inscription") }
    it { response.should have_selector('title', content: "Inscription") }
  end
  
  describe "index" do
    before do
      sign_in FactoryGirl.create(:user)
      FactoryGirl.create(:user, nom: "Zouli", email: "Zouli@example.com")
      FactoryGirl.create(:user, nom: "Ben", email: "ben@example.com")
      visit users_path+"?locale=fr"
    end
     after(:all)  { User.delete_all }

    it { response.should have_selector('title', content: 'Tous les utilisateurs') }
    it { response.should have_selector('h1',    content: 'Tous les utilisateurs') }
    end
    describe "index pagination" do
      before do
        sign_in FactoryGirl.create(:user)
         visit users_path+"?locale=fr"
      end
      before(:all) { 20.times { FactoryGirl.create(:user) } }
      after(:all)  { User.delete_all }

      it { response.should have_selector('div.pagination') }

      it "should list each user" do
        User.paginate(page: 1).first(20).each do |user|
            response.should have_selector('a', href: user_path(id: user, :locale=>"fr"), content: user.nom)
        end
      end
    end

  describe "delete links" do

      it { should_not have_selector('a', :content=> 'delete') }

      describe "as an admin user" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit users_path+"?locale=fr"
        end

        it { response.should have_selector('a', :content=> 'delete', href: user_path(User.first, :locale=>"fr")) }
        it "should be able to delete another user" do
          expect { click_link('delete') }.to change(User, :count).by(-1)
        end
        it { response.should_not have_selector('a', :content=> 'delete', href: user_path(admin, :locale=>"fr")) }
      end
    end
  #end
 # it "should list each user" do
# User.all.each do |user|
# response.should have_selector('h1', content: user.nom)
# end
# end
end
