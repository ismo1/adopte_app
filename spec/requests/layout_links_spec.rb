require 'spec_helper'
# describe "LayoutLinks" do
# describe "GET /layout_links" do
# it "works! (now write some real specs)" do
# # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
# get layout_links_index_path
# response.status.should be(200)
# end
# end
# end


describe "LayoutLinks" do

  it "devrait trouvé une page accueil '/'" do
    get '/', :locale => "fr"
    response.should have_selector('title', :content => "Description")
  end
  
  it "devrait trouvé une page contact '/contact'" do
    get '/contact', :locale => "fr"
    response.should have_selector('title', :content => "Contact")
  end
  
  it "devrait trouvé une page A Propos '/about'" do
    get '/about', :locale => "fr"
    response.should have_selector('title', :content => "À Propos")
  end
  
   it "devrait trouvé une page aide '/help'" do
    get '/help', :locale => "fr"
    response.should have_selector('title', :content => "Aide")
  end
 
  it "devrait trouvé une page d inscription '/signup'" do
    get '/signup', :locale => "fr"
    response.should have_selector('title', :content => "Inscription")
  end
  
  it "ces liens doivent trouver la bonne ppage" do
    visit root_path+"?locale=fr"
    
    click_link "À Propos"
    response.should have_selector('title', :content => "À Propos")
    
    click_link "Contact"
    response.should have_selector('title', :content => "Contact")
  
    click_link "Nouveautés"
    #response.should have_selector('title', :content => "Inscription")
  end
  
  
  
end