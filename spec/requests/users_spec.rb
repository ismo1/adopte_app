require 'spec_helper'

describe "Users" do

  describe "Test d'intégration de l'inscription" do
    describe "ratée" do
      it "ne devrait pas créer un nouvel utilisateur" do
        lambda do
          visit signup_path+"?locale=fr"
          fill_in 'Nom',          :with => ""
          fill_in 'E-mail',        :with => ""
          fill_in "Mot de Passe", :with => ""
          fill_in "Confirmation", :with => ""
          click_button "S'enregistrer"
          response.should render_template('users/new')
          response.should have_selector('div#error_explanation')
        end.should_not change(User, :count)
      end
    end

    describe "Réussi" do
      it " devrait créer un nouvel utilisateur" do
        lambda do
          visit signup_path+"?locale=fr"
          fill_in 'Nom',          :with => "nomlklk,lk"
          fill_in 'E-mail',        :with => "ism@lives.fr"
          fill_in "Mot de Passe", :with => "ism@live.fr"
          fill_in "Confirmation", :with => "ism@live.fr"
          click_button "S'enregistrer"
          # user = User.find_by_email("ism@live.fr")
          # # Sign in when not using Capybara as well.
          # cookies[:remember_token] = user.remember_token
          response.should be_success
          response.should render_template('users/show')
        end.should change(User, :count).by(1)
      end
    end
  end

  describe "registration with valid information" do

    it "ne devrait pas créer un nouvel utilisateur si ça n'existe pas" do
      @attr = { :nom => "Exemple_de_nom",
              :email => "Exemple@emaill.com",
              password: "password",
              password_confirmation: "password"
          }

      
        @user =  User.create!(@attr)
      new_name =  "New Name"
      new_email =  "new@example.com"

      visit users_path+ "/#{@user.id}/edit?locale=fr"
      fill_in "Nom",             with: new_name
      fill_in "E-mail",            with: new_email
      fill_in "Mot de Passe",         with: @user.password
      fill_in "Confirmation", with: @user.password_confirmation
      click_button "Enregistrer"

      response.should have_selector('title', :content => new_name)
      response.should have_selector('div.alert.alert-success')
      response.should have_selector('a', :content => 'Se déconnecter', href: signout_path+"?locale=fr")

      @user.reload.nom.should  == new_name
      @user.reload.email.should == new_email
    end
  end
end

