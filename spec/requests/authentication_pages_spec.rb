require 'spec_helper'

describe "Authentication Pages" do

  describe "signin page" do
    before {  visit signin_path+"?locale=fr"}

    it {  response.should have_selector('h1', :content => "S'identifier") }
    it { response.should have_selector('title', :content => "S'identifier") }
  end

  describe "sign in " do
    before {  visit signin_path+"?locale=fr"}

    describe "with invalid information" do
      before {click_button "Connexion" }
      it {  response.should have_selector('h1', :content => "S'identifier") }
      it { response.should have_selector('div.alert.alert-error') }

      describe "after visiting another page" do
        before { click_link "Contact" }
        it {response.should_not have_selector('div.alert.alert-error') }
      end

    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "E-mail",  with: user.email.upcase
        fill_in "Mot de Passe", with: user.password
        click_button "Connexion"
      end

      it {  response.should have_selector('title', :content => user.nom) }
      it { response.should have_selector('a', :content => "Profil") }
      it { response.should have_selector('a', :content =>'Paramètres', href: edit_user_path(id: user, :locale=>"fr")) }
      it { response.should have_selector('a', :content =>'Se déconnecter', href: signout_path+"?locale=fr") }
      it { response.should_not have_selector('a', :content =>'Connexion', href: signin_path) }
    end
  end

  describe "Authorization" do

    describe "for non-signed-in users" do
      let(:user) { FactoryGirl.create(:user) }

      describe "in the Users controller" do

        describe "visiting the edit page" do
          before { visit edit_user_path(id: user, :locale=>"fr") }
          it { response.should have_selector('title', content: "S'identifier") }
        end

        describe "submitting to the update action" do
          before { put user_path(id: user, :locale=>"fr") }
          specify { response.should redirect_to(signin_path+"?locale=fr") }
        end

      end
      describe "edit" do
        let(:user) { FactoryGirl.create(:user) }
        before do
          sign_in user
          visit edit_user_path(user)
        end
      end
    end
    
    # This creates a user with a different email address from the default. 
    # The tests specify that the original user should not have access 
    # to the wrong user’s edit or update actions.  
     describe "as wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before { sign_in user }

      describe "visiting Users#edit page" do
        before { visit edit_user_path(id: wrong_user,:locale=>"fr")}
        it { should_not have_selector('title', content: "Mise à jour du Profil") }
      end

      describe "submitting a PUT request to the Users#update action" do
        before { put user_path(id: wrong_user, :locale=>"fr") }
        specify { response.should redirect_to(root_path+"?locale=fr") }
      end
    end
  end
  
 #  Friendly forwarding
 # Our site authorization is complete as written, but there is one minor blemish:
  # when users try to access a protected page, they are currently redirected to their
  # profile pages regardless of where they were trying to go. In other words,
  # if a non-logged-in user tries to visit the edit page, after signing in the
  # user will be redirected to /users/1 instead of /users/1/edit. It would be much
  # friendlier to redirect them to their intended destination instead.
  describe "for non-signed-in users" do
      let(:user) { FactoryGirl.create(:user) }
     
      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(id: user, :locale=>"fr")
          fill_in "E-mail",    with: user.email
          fill_in "Mot de Passe", with: user.password
          click_button "Connexion"
        end

        describe "after signing in" do

          it "should render the desired protected page" do
            response.should have_selector('title', content: "Mise à jour - Profil")
          end
        end
     end
      describe "visiting the user index" do
          before { visit users_path+"?locale=fr" }
          it { response.should have_selector('title', content: "S'identifier") }
        end
    end
    
    describe "as non-admin user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:non_admin) { FactoryGirl.create(:user) }

      before { sign_in non_admin }

      describe "submitting a DELETE request to the Users#destroy action" do
        before { delete user_path :id => user, :locale=>"fr" }
        specify { response.should redirect_to(root_path+"?locale=fr") }        
      end
    end
end