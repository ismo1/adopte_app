require 'spec_helper'

describe "searches/show" do
  before(:each) do
    @search = assign(:search, stub_model(Search,
      :keywords => "Keywords",
      :category_id => 1,
      :user_id => 2,
      :minimum_price => 1.5,
      :maximum_price => 1.5,
      :status => "Status",
      :description => "Description",
      :new => "New",
      :show => "Show"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Keywords/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/Status/)
    rendered.should match(/Description/)
    rendered.should match(/New/)
    rendered.should match(/Show/)
  end
end
