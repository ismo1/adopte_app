require 'spec_helper'

describe "searches/index" do
  before(:each) do
    assign(:searches, [
      stub_model(Search,
        :keywords => "Keywords",
        :category_id => 1,
        :user_id => 2,
        :minimum_price => 1.5,
        :maximum_price => 1.5,
        :status => "Status",
        :description => "Description",
        :new => "New",
        :show => "Show"
      ),
      stub_model(Search,
        :keywords => "Keywords",
        :category_id => 1,
        :user_id => 2,
        :minimum_price => 1.5,
        :maximum_price => 1.5,
        :status => "Status",
        :description => "Description",
        :new => "New",
        :show => "Show"
      )
    ])
  end

  it "renders a list of searches" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Keywords".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "New".to_s, :count => 2
    assert_select "tr>td", :text => "Show".to_s, :count => 2
  end
end
