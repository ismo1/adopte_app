require 'spec_helper'

describe "searches/edit" do
  before(:each) do
    @search = assign(:search, stub_model(Search,
      :keywords => "MyString",
      :category_id => 1,
      :user_id => 1,
      :minimum_price => 1.5,
      :maximum_price => 1.5,
      :status => "MyString",
      :description => "MyString",
      :new => "MyString",
      :show => "MyString"
    ))
  end

  it "renders the edit search form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", search_path(@search), "post" do
      assert_select "input#search_keywords[name=?]", "search[keywords]"
      assert_select "input#search_category_id[name=?]", "search[category_id]"
      assert_select "input#search_user_id[name=?]", "search[user_id]"
      assert_select "input#search_minimum_price[name=?]", "search[minimum_price]"
      assert_select "input#search_maximum_price[name=?]", "search[maximum_price]"
      assert_select "input#search_status[name=?]", "search[status]"
      assert_select "input#search_description[name=?]", "search[description]"
      assert_select "input#search_new[name=?]", "search[new]"
      assert_select "input#search_show[name=?]", "search[show]"
    end
  end
end
