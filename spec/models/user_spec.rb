# == Schema Information
#
# Table name: users
#
#  id                     :integer         not null, primary key
#  nom                    :string(255)
#  email                  :string(255)
#  salt                   :string(255)
#  encrypted_password     :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  remember_token         :string(255)
#  admin                  :boolean         default(FALSE)
#  auth_token             :string(255)
#  password_reset_token   :string(255)
#  password_reset_sent_at :datetime
#

require 'spec_helper'

describe User do
#pending "add some examples to (or delete) #{__FILE__}"

  before(:each) do
    @attr = { :nom => "Exemple_de_nom", 
              :email => "Exemple@email.com",
              password: "password",
              password_confirmation: "password" 
            }
  
  end

  it "devrait créer une nouvelle instance dotée d'attributs valides" do
     User.create!(@attr)
  end

  it "devrait exigé un nom" do
    bad_user = User.new(@attr.merge(:nom => ""))
    bad_user.should_not be_valid
  end

  it "exige une adresse email" do
    no_user_email = User.new(@attr.merge(:email => ""))
    no_user_email.should_not be_valid
  end
  
  def generateRadomPass(size)
   a = 'a'..'z'
    a = a.to_a.shuffle
    b = 'A'..'Z'
    b= b.to_a.shuffle
    c = 0..9
    c = c.to_a.shuffle
    e = ['-','_','%','?','&','@','#']
    e.shuffle
    d = Array.new
    d << a << b << c << e
    pass = Array.new
    i=0
    d.to_a.each do |itemArray|
      itemArray.to_a.each do |item| 
        pass[i] = item
        i +=1
      end
    end 
     puts pass.map
     pass = pass.shuffle[0..size].join
  end

  it "devrait rejeter les noms dont la longeur est supérieur à 50" do
    bad_user_with_bad_legnth_name = User.new(@attr.merge(:nom => generateRadomPass(51)))
    bad_user_with_bad_legnth_name.should_not be_valid
  end

  it "devrait accepter les addresse email valides" do
    adresses = %w[ user.foo@exemple.com user@exemple.com user_foo_@exemple.com.net ]
    adresses.each do |adresse|
      valid_user_email = User.new(@attr.merge(:email => adresse))
      valid_user_email.should be_valid
    end
  end

  it " ne devrait pas accepter les addresse email non valides" do
    adresses = %w[ user_foo@exemple,com user@exemple. user_foo_at_exemple.com.net ]
    adresses.each do |adresse|
      invalid_user_email = User.new(@attr.merge(:email => adresse))
      invalid_user_email.should_not be_valid
    end
  end

  it "devrait rejeter les utilisateurs avec un email en doublons dans la base de données" do
    User.create!(@attr)
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end
  
  it "devrait rejeter les utilisateurs avec un email en doublons sans conciderer la casse car EMAIL@exemple.com == email@exemple.com == eMaiL@EXEMPLE.COM" do
    upcased_email = @attr[:email].upcase
    User.create!(@attr.merge(:email => upcased_email))
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end
  
   it "devrait exiger la confirmation du password" do
    pass1 = generateRadomPass(10)
    pass2 = generateRadomPass(9)
    user_with_valid_password = User.new(@attr.merge(password: "#{pass1}", password_confirmation: "#{pass2}"))
    user_with_valid_password.should_not be_valid
  end
  
  it "devrait accepter la confirmation du password" do
    pass = generateRadomPass(10)
    user_with_valid_password = User.new(@attr.merge(password: "#{pass}", password_confirmation: "#{pass}"))
    user_with_valid_password.should be_valid
  end
  
  it "devrait exiger un password non vide" do
    user_with_blank_password = User.new(@attr.merge(password: '', password_confirmation: ''))
    user_with_blank_password.should_not be_valid
  end
  
   it "devrait rejetter les passwords courts" do
     pass = generateRadomPass(4)
    user_with_short_password = User.new(@attr.merge(password: "#{pass}", password_confirmation: "#{pass}"))
    user_with_short_password.should_not be_valid
  end
  
  it "devrait rejetter les passwords trop longs" do
    pass = generateRadomPass(50)
    user_with_long_password = User.new(@attr.merge(password: "#{pass}", password_confirmation: "#{pass}"))
    user_with_long_password.should_not be_valid
  end


  describe "Cryptage du mot de passe" do
    
      before(:each) do
        @user = User.create!(@attr)
      end
      
    it "devrait avoir un attribut  mot de passe crypté" do
        
        @user.should respond_to(:encrypted_password)
      end
      
       it "devrait avoir un jetton de session " do
         @user.should respond_to(:remember_token)
      end
      
       it "devrait avoir un champ admi " do
         @user.should respond_to(:admin)
      end
      
       # it "devrait avoir une authentification d'admin " do
         # @user.should respond_to(:authenticate)
      # end
      
       it "le User doit être valide " do
         @user.should be_valid
      end
      
      it "le User doit pas être un administrateur " do
         @user.should_not be_admin
      end
    
      describe "with admin attribute set to 'true'" do
        before do
          @user.save!
          @user.toggle!(:admin)
        end
    
        it { @user.should be_admin }
      end
    
     describe "Méthode d'authentification" do
      
        it "devrait retourner null en cas d'inéquation entre email/mot de passe" do
         wrong_password_user = User.authenticate(@attr[:email], "fjlkls")
         wrong_password_user.should_not be_valid
      end
      
      it "devrait retourner nil quand un email ne correspond à aucun utilisateur" do
        nonexistent_user = User.authenticate("bar@foo.com", @attr[:password])
        nonexistent_user.should be_nil
      end
     
      it "devrait retourner l'utilisateur si email/mot de passe correspondent" do
        matching_user = User.authenticate(@attr[:email], @attr[:password])
        matching_user.should == @user
       end
    end
    
      # describe "remember token" do
        # before {
             # @user = User.create!(@attr)
             # }
        # it "ne doit pas nil " do
          # @user.remember_token.should_not be_nil
        # end
      # end
end
end 
