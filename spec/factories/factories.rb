# # En utilisant le symbole ':user', nous faisons que
# # Factory Girl simule un modèle User.
# Factory.define :user do |user|
# user.nom                  "Michael Hartl"
# user.email                 "mhartl@example.com"
# user.password              "foobar"
# user.password_confirmation "foobar"
# end

require 'factory_girl'

#
# FactoryGirl.define do
# factory :user do |user1|
# user1.nom                  "Michael Hartl"
# user1.email                 "mhartl@example.com"
# user1.password              "foobar"
# user1.password_confirmation "foobar"
# end
# end

FactoryGirl.define do
  factory :user do
    sequence(:nom)  { |n| "Person #{n}" }
    sequence(:email) { |n| "person_#{n}@example.com"}
    password "foobar"
    password_confirmation "foobar"

    factory :admin do
      admin true
    end
  end
end

