require 'spec_helper'

describe TestJsController do

  describe "GET 'test1'" do
    it "returns http success" do
      get 'test1', :locale => "fr"
      response.should be_success
     
    end
  end

  describe "GET 'test2'" do
    it "returns http success" do
      get 'test2', :locale => "fr"
      response.should be_success
    end
  end

end
