require 'spec_helper'

describe DataFileController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'uploadFile'" do
    it "returns http success" do
      get 'uploadFile'
      response.should be_success
    end
  end

end
