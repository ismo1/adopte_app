require 'spec_helper'

describe PagesController do
  render_views
  
   before(:each) do
    #
    # Define @base_title here.
    #
    
    @base_title = "Adopte 1 Coloc Web-App"
  end
  
  describe "GET 'home'" do
    it "returns http success" do
      get 'home', :locale => "fr"
      response.should be_success
    end
    
    it "doit avoir le bon titre" do
      get 'home', :locale => "fr"
      response.should have_selector("title", 
            :content => @base_title + " | Accueil")
    end
  end

  describe "GET 'contact'" do
    it "returns http success" do
      get 'contact', :locale => "fr"
      response.should be_success
    end
    
     it "doit avoir le bon titre" do
      get 'contact', :locale => "fr"
      response.should have_selector("title", 
            :content =>  @base_title +  " | Contact")
    end
  end
  
  describe "GET 'about'" do
    it "returns http success" do
      get 'about', :locale => "fr"
      response.should be_success
    end
    
     it "doit avoir le bon titre" do
      get 'about', :locale => "fr"
      response.should have_selector("title", 
            :content =>  @base_title + " | À Propos")
    end
  end
  
  describe "GET 'help'" do
    it "returns http success" do
      get 'help', :locale => "fr"
      response.should be_success
    end
    
     it "doit avoir le bon titre" do
      get 'help', :locale => "fr"
      response.should have_selector("title", 
            :content =>  @base_title + " | Aide")
    end
  end
  
  describe "GET 'site'" do
    it "returns http success" do
      get 'site', :locale => "fr"
      response.should be_success
    end
    
     it "doit avoir le bon titre" do
      get 'site', :locale => "fr"
      response.should have_selector("title", 
            :content =>  @base_title + " | Description")
    end
  end
  
end
