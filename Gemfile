source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0.rc1'

# Use postgresql as the database for Active Record
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0.rc1'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.0.1'

#permet de gérer des avatars pour les utilisateurs
gem 'gravatar_image_tag'

#A small effort in making a plugin which helps you detect
#the users preferred language, as sent by the HTTP header.
gem 'http_accept_language'

gem 'unicorn'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  # gem 'sdoc', require: false
end

#in rails-4, ActiveModel::MassAssignmentSecurity need this gem for attr_accessible
gem 'protected_attributes'

group :development do
  gem 'rspec-rails' #, '2.5.0'
 gem 'annotate', ' ~> 2.4.1.beta1'
 gem 'rails-erd'
 gem "nifty-generators"
end

gem "thin"
gem 'rails_autocomplete'

group :development, :test do
  #run rake diagram:all for generate all diagrams
  gem 'railroady'
end

group :test do
  gem 'rspec' #, '2.5.0'
  gem 'rspec-rails'
  gem 'webrat' #, '0.7.1'
  gem 'spork'
  #gem 'factory_girl_rails'
  #gem 'ZenTest'
  #gem 'autotest-rails'
  #gem 'autotest-fsevent'
  #gem 'autotest-growl'
  gem 'capybara'
end

gem "rack-ssl", "~> 1.3.3"

#Bootstrap css
gem 'bootstrap-sass'

#gem pour le cryptage et l'authentifactions
gem 'bcrypt-ruby', '~> 3.0.1'

#gem capable de generer des Noms d'utilisateurs proche de la réalité pour de tests
gem 'faker', '~> 1.0.1'

gem "populator"

#Gem Pour la Pagination
gem 'will_paginate'

#Gem configurant celui de la Pagination utiliser bootsrtap
gem 'bootstrap-will_paginate'

#beadCrumb
gem "breadcrumbs_on_rails"

#gem "activeadmin"

#gem 'jquery-rails', '2.3.0'


gem 'devise',              github: 'plataformatec/devise',     branch: 'rails4'
gem 'responders',          github: 'plataformatec/responders'
gem 'inherited_resources', github: 'josevalim/inherited_resources'
gem 'ransack',             github: 'ernie/ransack',            branch: 'rails-4'
gem 'activeadmin',         github: 'akashkamboj/active_admin', branch: 'rails4'
gem 'formtastic',          github: 'justinfrench/formtastic', branch: 'rails4beta'


gem 'rails-timeago'

gem 'bootstrap-datepicker-rails', :require => 'bootstrap-datepicker-rails',
                              :git => 'git://github.com/Nerian/bootstrap-datepicker-rails.git'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
