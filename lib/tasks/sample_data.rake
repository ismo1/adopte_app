namespace :db do
  desc "remplir la Bdd avec ces exemples d'utilisateurs"
  task populate: :environment do
     require 'populator'
     require 'faker'
     
    make_users
    make_microposts
    make_relationships
    make_orders
    make_products
  end
end

def make_users
  user = User.find_by_email("ben.zouli@rails.fr")
    user.destroy unless user.nil?
    
    user = User.find_by_email("ismo1@rails.fr")
    user.destroy unless user.nil?
    99.times do |n|
      user = User.find_by_email("user_n-#{n+1}@rails.fr")
      user.destroy unless user.nil?
      user = User.where("email like ?", "%#{n+1}@rails.fr")
      user.destroy_all unless user.nil?
    end
    admin1 = User.create!(nom: "Ben Zouli",
                 email: "ben.zouli@rails.fr",
                 password: "foobar",
                 password_confirmation: "foobar")
    admin1.toggle!(:admin)
    
    admin = User.create!(nom: "ismo1",
                 email: "ismo1@rails.fr",
                 password: "foobar",
                 password_confirmation: "foobar")
    admin.toggle!(:admin)
    
    
    email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
     
    99.times do |n|
      name  = Faker::Name.name
      name_for_email = name.split.join << "#{n}@rails.fr"
      email = (email_regex.match(name_for_email))?  name_for_email : "user_n-#{n+1}@rails.fr"
      password  = "password"
      User.create!(nom: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
end

def make_microposts
  users = User.all(limit: 6)
    50.times do
      content = Faker::Lorem.sentence(5)
      users.each { |user_with_microposts| user_with_microposts.microposts.create!(content: content) }
    end
end

def make_relationships
  users = User.all
  user  = users.first
  followed_users = users[2..40]
  followers      = users[3..60]
  followed_users.each { |followed| user.follow!(followed) } #user suit (est abonné avec) les autres followed
  followers.each      { |follower| follower.follow!(user) } #user est suivit (fait a été ajouté comme abonnée par ) par les autres followers
end


def make_orders
  
     email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
     
  99.times do |n|
      order = Order.find_by_email("CLient-#{n+1}@rails.com")
      order.destroy unless order.nil?
    end
  Order.transaction do
  99.times do |i|
    i+=1
    name = Faker::Name.name
     name_for_email = name.split.join << "#{i}@rails.fr"
    fake_mail   = (email_regex.match(name_for_email))?  name_for_email : "user_n-#{i+1}@rails.fr"
   # :name, :last_name, :address, :city, :code_postal, :email, :email_confirmation
    Order.create!(name: name, last_name: Faker::Company.name , 
                  address:  Faker::Address.street_address, 
                  city: Faker::Address.city, 
                  code_postal: "59300",
                  email: fake_mail, 
                  email_confirmation: fake_mail,
                  pay_type: "Carte de Crédit")
    end
  end
end



# namespace :db do
  # desc "Erase and fill database"
  # task :populate => :environment do
    # require 'populator'
    # require 'faker'
#     
    # [Category, Product, Person].each(&:delete_all)
#     
    # Category.populate 20 do |category|
      # category.name = Populator.words(1..3).titleize
      # Product.populate 10..100 do |product|
        # product.category_id = category.id
        # product.name = Populator.words(1..5).titleize
        # product.description = Populator.sentences(2..10)
        # product.price = [4.99, 19.95, 100]
        # product.created_at = 2.years.ago..Time.now
      # end
    # end
#     
    # # Person.populate 100 do |person|
      # # person.name    = Faker::Name.name
      # # person.company = Faker::Company.name
      # # person.email   = Faker::Internet.email
      # # #person.phone   = Faker::PhoneNumber.phone_number
      # # person.street  = Faker::Address.street_address
      # # person.city    = Faker::Address.city
      # # #person.state   = Faker::Address.us_state_abbr
      # # #person.zip     = Faker::Address.zip_code
    # # end
  # end
# end
# 


def make_products
   [Category, Product].each(&:delete_all)
   #categories = Array.new
   categories = %w(Jeux Livres CD/DVD Immobilier Voyage Cuisine)
   images = %w( my_shop128.png 3000.jpg 3453.jpg 3680.jpg android.png java.png phone.png rails.png voyage.png )
   i=0
   Category.populate categories.length do |category|
      category.description = Populator.sentences(2..3)
      category.name = categories[i]
      i+=1
      Product.populate 10..15 do |product|
        product.category_id = category.id
        product.title = Populator.words(2..4).titleize
        product.description = Populator.sentences(2..5)
        product.price = [4.99, 19.95, 100, 20.50, 31.80, 67.90, 10.78, 56.34, 15.99 ]
        product.created_at = 2.days.ago..Time.now
        product.status = Product::PRODUCT_STATUS.sample  
        product.image_url = images.sample
        product.user_id = User.first(:offset => rand(5))
      end
    end
end
