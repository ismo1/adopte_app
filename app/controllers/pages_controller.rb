class PagesController < ApplicationController
  skip_before_filter :authorize, only: [:home, :contact,:about, :help , :search]  #je laisse le controller lui même s'occuper de ses verification
  def index
    render :site
  end

  def home
    @titre = t :home
    @serach_comp =  display_search_component?
    add_breadcrumb t :home, :root_path

    if signed_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page],:per_page => 10)
      
      @product  = current_user.products.build
      @feed_product = current_user.feed_product.paginate(page: params[:page],:per_page => 10)
    end
  end

  def contact
    @titre = "Contact"
    @serach_comp =  display_search_component?
    add_breadcrumb "contact", :contact_path
  end

  def about
    @titre = t :about
    @serach_comp =  display_search_component?
    add_breadcrumb t :about, :about_path
  end

  def help
    @titre = t :help
    @serach_comp =  display_search_component?
    add_breadcrumb  t :help, :help_path
  end

  def site
    @titre = "Description"
    @serach_comp =  display_search_component?
  #flash[:notice] = t(:hello_flash)
  end

  #********************* Serach ******************
  def search
    # if !params[:search].nil?
    if  !params[:user].nil?
      if signed_in?
        @users = User.search(params[:search])
      end
    end

    if !params[:top_product].nil?
      @keywords = params[:top_product]
      @products = Product.where("lower(products.title) LIKE ? OR lower(products.status) LIKE ?", "%#{@keywords}%".downcase, "%#{@keywords}%".downcase).paginate(page: params[:page],:per_page=> 10)
    else

   # if  !params[:product].nil?
      @keywords = params[:keywords].downcase unless params[:keywords].nil?
      @minimum_price = params[:minimum_price]  unless params[:minimum_price].nil?
      @maximum_price = params[:maximum_price] unless params[:maximum_price].nil?
      @category_id = params[:category_id]  unless params[:category_id].nil?
      @user_id = params[:user_id]  unless params[:category_id].nil?
      @description = params[:description].downcase  unless params[:description].nil?
      @status1 = params[:status1].downcase  unless params[:status1].nil?
      @status3 = params[:status2].downcase  unless params[:status2].nil?
      @status2 = params[:status3].downcase  unless params[:status3].nil?
 
      @products ||= find_products.paginate(page: params[:page],:per_page=> 10)
    end

    @titre = t :search_result
  # end
  end

  def products
    @products ||= find_products
  end

  private

  def find_products
    Product.all(:conditions => conditions)
    #Product.where(:conditions => conditions).load
  end

  def keyword_conditions
    ["lower(products.title) LIKE ?", "%#{@keywords}%"] unless @keywords.blank?
  end

  def category_conditions
    ["products.category_id = ?", @category_id] unless @category_id.blank?
  end

  def user_conditions
    ["products.user_id = ?", @user_id] unless @user_id.blank?
  end

  def minimum_price_conditions
    ["products.price >= ?", @minimum_price] unless @minimum_price.blank?
  end

  def maximum_price_conditions
    ["products.price <= ?", @maximum_price] unless @maximum_price.blank?
  end

  def status_conditions
    ["lower(products.status) LIKE ? OR products.status LIKE ? OR products.status LIKE ?", "%#{@status1}%", "%#{@status2}%", "%#{@status3}%"]
  end

  def description_conditions
    ["lower(products.description) LIKE ?", "%#{@description}%"] unless @description.blank?
  end

  def conditions
    [conditions_clauses.join(' AND '), *conditions_options]
  end

  def conditions_clauses
    conditions_parts.map { |condition| condition.first }
  end

  def conditions_options
    conditions_parts.map { |condition| condition[1..-1] }.flatten
  end

  def conditions_parts
    private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
  end

end