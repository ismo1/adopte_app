class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authorize #lbloque l'accès aux methodes sans authentifications
  #protect_from_forgery

  # By default, all the helpers are available in the views
  # but not in the controllers.
  # We need the methods from the Sessions helper in both places,
  # so we have to include it explicitl
  include SessionsHelper

  before_filter :set_i18n_locale
  #*********************************************************
  def set_i18n_locale
    unless params[:locale]
      params[:locale] = extract_locale_from_accept_language_header
    end
    @available = %w(fr en)
    if @available.include? params[:locale]
      I18n.locale = params[:locale]
    else
      flash.now[:notice] = "#{params[:locale]} translation not available"
          logger.error flash.now[:notice]
      I18n.locale = I18n.default_locale
    end
  end

  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  # Every helper method dependent on url_for (e.g. helpers for named routes
  # like root_path or root_url, resource routes like books_path or books_url, etc.)
  # will now automatically include the locale in the query string, like this:
  # http://localhost:3001/?locale=ja.

  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{options.inspect}\n"
    { :locale => I18n.locale }
  end
  

# def set_i18n_locale_from_params
      # if params[:locale]
        # if I18n.available_locales.include?(params[:locale].to_sym)
          # I18n.locale = params[:locale]
        # else
          # flash.now[:notice] = 
            # "#{params[:locale]} translation not available"
          # logger.error flash.now[:notice]
        # end
      # end
    # end

  #********************************************************

  def display_search_component?
    return true if @titre != 'Accueil' && @titre != 'Description'
    false
  end



  #*********************************************************

  # Force signout to prevent CSRF attacks
  def handle_unverified_request
    sign_out
    super
  end
end
