class OrdersController < ApplicationController
  skip_before_filter :authorize, only: [:new, :create]
  #before_filter :correct_user,   only: :index
  before_filter :admin_user,     only: [:edit, :update, :destroy]
  # GET /orders
  # GET /orders.json
  def index
    #@orders = Order.all
    if current_user.admin?
      @orders = Order.paginate page: params[:page], order: 'created_at desc', per_page: 10
    else
      @orders = Order.current_user_orders(current_user.id)
      if !@orders.blank?
         @orders = @orders.paginate page: params[:page], order: 'created_at desc', per_page: 10
      end

    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
      format.xml { render xml: @orders }
    end
  end


  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @cart = current_cart
    if @cart.line_items.empty?
      redirect_to store_url
      flash[:notice] = t :empty_cart
    return
    end

    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
      format.js
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(params[:order])
    @order.add_line_items_from_cart(current_cart)

    respond_to do |format|
      if @order.save
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        #envoie de mail au client pour la confirmation
        OrderNotifier.received(@order).deliver
        format.html { redirect_to store_url}
        flash[:success] = t :order_received

        format.json { render json: @order, status: :created,
          location: @order }
      else
        @cart = current_cart
        format.html { render action: "new" }
        format.json { render json: @order.errors,
          status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order }
        flash[:success] = t :order_success_update

        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :no_content }
    end
  end

  def orders_by_date
    @orders_by_date = Order. paginate(page: params[:page], order: 'created_at desc', per_page: 10, :conditions => ["created_at >= :start_date AND created_at <= :end_date",
             { :start_date => params[:start_date], :end_date => params[:end_date] }])

    @start_date = params[:start_date]
    @end_date = params[:end_date]
    #render 'orders_by_date'
    respond_to do |format|
      format.html #{ redirect_to orders_url }
      format.json #{ head :no_content }
      format.js
    end
  end

  def admin_user
    if !current_user.admin?
      redirect_to(root_path)
      flash[:errors] = 'Action non autorisée!'
    end
  end
end