class StoreController < ApplicationController
  skip_before_filter :authorize #permet d'autoriser l'accès à ces methodes sans authentifications
  def index
    if params[:set_locale]
      redirect_to store_path(locale: params[:set_locale])
    else
      @products = Product.order(:title).paginate(page: params[:page],:per_page=> 10)
      @cart = current_cart
      #@cart_count  = current_cart.line_items.count
    end
    
  end
end
