class ProductsController < InheritedResources::Base
  skip_before_filter :authorize, only:[:show]
  before_filter :true_owner,   only: [:update,:destroy, :who_bought, :edit]
  # GET /products
  # GET /products.json
  def index
    if current_user.admin?
      @products = Product.paginate(page: params[:page],:per_page=> 10)
    else
      @products = Product.where(:user_id => current_user.id).paginate(page: params[:page],:per_page=> 10)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
      format.js
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])
    @remote_product = @product
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
      # format.js { render js: "show_product"}
      format.js
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
  end

  # POST /products
  # POST /products.json
  def create

    @product = current_user.products.build(params[:product])
    #@product = Product.new(params[:product])

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product}
        flash[:success] = 'Le produit à bien été ahouté.'
        format.json { render json: @product, status: :created,
          location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors,
          status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product}
        flash[:success] = t :product_update_success
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors,
          status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end

  def who_bought
    @product = Product.find(params[:id])
    respond_to do |format|
      format.html
      format.atom
      format.json { render json: @product}
      format.xml { render xml: @product}
    end
  end

  private

  def true_owner
    @product = current_user.products.find_by_id(params[:id])
    #@micropost.any? ? @old_micropost = @micropost : @old_micropost = []
    if @product.nil? && !current_user.admin?
      redirect_to :home
      flash[:errors] = 'Action non autorisée!'
    end
  end

end
