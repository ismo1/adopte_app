class UsersController < ApplicationController
  skip_before_filter :authorize, only: [:new, :create]
  # before_filter :signed_in_user, only: [:index, :show, :edit, :update, :following, :followers]
  before_filter :correct_user,   only: [:edit, :update]
  before_filter :admin_user,     only: [:destroy, :update_profile]
  before_filter :skip_password_attribute, only: [:update_profile]
  # GET /users
  # GET /users.json
  # GET /users.xml
  def index
    @titre = t :all_users
    @users_json = User.all
    @users = User.paginate(page: params[:page],:per_page => 20 )
    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json # index.json.erb
      #index.js.erb
      # format.json { render json: @user }
      format.xml { render xml: @users}
    end
  end

  def new
    @titre = t :signup
    # @serach_comp =  display_search_component?
    add_breadcrumb t :signup, :signup_path
    @user = User.new
  end

  # GET /users/1
  # GET /users/1.json
  # GET /users/1.xml
  def show
    @user = User.find(params[:id])
    @titre = @user.nom
    @microposts = @user.microposts.paginate(page: params[:page])
    #@users = User.all
    #@cpt = @user.kind_of?(Array) ? @user.count : 1
    respond_to do |format|
      format.html # show.html.erb
      format.json # show.json.erb
      # format.json { render json: @user }
      format.xml { render xml: @user}
    end
  end

  def create
    @titre = t :signup
    add_breadcrumb t :signup, :signup_path
    @user = User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success] = t :welcome_message_after_sign_up
      redirect_to signin_url
    else
      render :new
    end
  end

  def edit
    @titre = t :edit_user
    @user = User.find(params[:id])
  end
  
  def edit_profile
    @titre = "#{t :edit_user} | by Admin "
    @user = User.find(params[:id])
     render :edit_profile
  end



  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      #flash[:info]
      flash[:success] = t :edit_user_success, :name => @user.nom.upcase
      sign_in @user
      redirect_to @user
    else
      render :edit
    end
  end
  
   def update_profile
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      #flash[:info]
      flash[:success] = t :edit_user_success, :name => @user.nom.upcase
      redirect_to users_path
    else
      render :edit_profile
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = t :delete_user_success
    redirect_to users_url
  end

  def following
    @titre = "Following"
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:users_page])
    @users_sidebar = @users.paginate(page: params[:users_sidebar_page], :per_page => 40)
    render 'show_follow'
  end

  def followers
    @titre = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:users_page])
    @users_sidebar = @users.paginate(page: params[:users_sidebar_page], :per_page => 40)
    render 'show_follow'
  end

  def autocomplete
    @users= User.select("id, nom").where("lower(nom) LIKE ?", "%#{params[:term]}%".downcase).limit(20)
    @hash = []
    @users.each do |user|
      @hash << {"label" => user.nom, "id" => user.id}
    end
    puts @hash.to_a
    render :json => @hash

  end

  private

  def admin_user
    if !current_user.admin?
      redirect_to(root_path)
      flash[:errors] = 'Action non autorisée!'
    end
  end

  def correct_user
    @user = User.find(params[:id])
    if !current_user?(@user) &&  !current_user.admin?
      redirect_to root_path
    end
  end
  private
  def skip_password_attribute
    if params[:password].blank? && params[:password_confirmation].blank?
      params.except!(:password, :password_confirmation)
    end
  end

end
