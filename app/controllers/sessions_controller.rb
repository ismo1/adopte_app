class SessionsController < ApplicationController
  skip_before_filter :authorize
  def new
    @titre = t :signin
  end
  
  # def create
    # user = User.find_by_email(params[:session][:email].downcase)
    # if user && user.authenticate(params[:session][:password])
     # sign_in user
      # redirect_to user
    # else
     # flash[:error] = t :sign_in_error # Not quite right!
      # render :new
    # end
  # end
  
  def create
    user = User.authenticate(params[:session][:email], params[:session][:password])
    if user
     sign_in user
     # flash[:success] =  "#{t :welcome_in_profile} #{user.nom}!" 
      flash[:success] = "#{t :welcome_in_profile} #{user.nom}!" unless stored_location?
      
      if params[:remember_me]
      cookies.permanent[:auth_token] = user.auth_token
    else
      cookies[:auth_token] = user.auth_token
    end
    
    redirect_back_or store_url
     # redirect_back_or user #on redirige le user dans sa requête initiale 
      #si il en eexiste sinon il ira directe dans son profile
    else
     flash.now[:error] = t :sign_in_error # Not quite right!
      render :new
    end
  end

  def destroy
    sign_out
    flash.now[:notice] = "Vous êtes déconnecté" 
   redirect_to store_path
  end
end