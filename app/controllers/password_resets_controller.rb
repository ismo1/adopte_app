class PasswordResetsController < ApplicationController
  skip_before_filter :authorize
  
  def new
  end
  
  def create
    user = User.find_by_email(params[:email])
    user.send_password_reset if user
    redirect_to root_url
    flash.now[:notice]  = t :reset_password 
    # a mail sent with password reset instructions
  end

  def edit
    @user = User.find_by_password_reset_token!(params[:id])
  end

  def update
    @user = User.find_by_password_reset_token(params[:id])
    if @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path
      flash[:alert] = t :password_expired
    elsif @user.update_attributes(params[:user])
      redirect_to root_url
       flash[:success] = t :password_has_ben_reset 
    else
      render :edit
    end
  end
end
