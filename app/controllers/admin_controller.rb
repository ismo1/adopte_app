class AdminController < ApplicationController
  def index
    @total_orders = Order.count
    @total_orders_from_1_days =  Order.count(:conditions => ["created_at >= :start_date AND created_at <= :end_date", 
           { :start_date => 1.day.ago, :end_date => Time.now }])
  end
end
