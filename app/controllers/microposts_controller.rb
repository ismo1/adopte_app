class MicropostsController < ApplicationController
  skip_before_filter :authorize   #je laisse le controller lui même s'occuper de ses verification
  before_filter :signed_in_user, only: [:create, :destroy]
  before_filter :correct_micropost_user,   only: :destroy
  def index
  end

  def create
    @micropost = current_user.microposts.build(params[:micropost])
    if @micropost.save
      flash[:success] = t :create_post
      respond_to do |format|
        format.html { redirect_to :home }
       # format.js
      end
    else
    #@feed_items = @old_micropost #on lui affecte un vide ou l'anciennes valeur
      @feed_items = []
      render 'pages/home'
    #redirect_to :home
    end
  end

  def destroy
    @micropost.destroy
    respond_to do |format|
      format.html { redirect_to :home }
     # format.js
    end
  end

  private

  def correct_micropost_user
    @micropost = current_user.microposts.find_by_id(params[:id])
    #@micropost.any? ? @old_micropost = @micropost : @old_micropost = []
     if @micropost.nil?
       redirect_to :home
       flash[:errors] = 'Action non autorisée!'
     end
  end
end
