class CategoriesController < InheritedResources::Base
  before_filter :admin_user,   only: :destroy
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all
    @titre = "Categories"
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @categories }
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @category = Category.find(params[:id])
    @titre = "Category | " + @category.name
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/new
  # GET /categories/new.json
  def new
    @category = Category.new
    @titre = "Category | New"
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/1/edit
  def edit
    @titre = "Category | Edit"
    @category = Category.find(params[:id])
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(params[:category])

    respond_to do |format|
      if @category.save
        format.html { redirect_to categories_url}
        flash.now[:success] = t :success_create_category
        format.json { render json: @category , status: :created,
          location: @category }
      else
        format.html { render action: "new" }
        format.json { render json: @category.errors,
          status: :unprocessable_entity }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update
    @category = Category.find(params[:id])

    respond_to do |format|
      if @category.update_attributes(params[:category])
        format.html { redirect_to @category}
        flash.now[:success] = t :success_update_category
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @category.errors,
          status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url }
      format.json { head :no_content }
    end
  end

  def admin_user
    if !current_user.admin?
      redirect_to(root_path)
      flash[:errors] = 'Action non autorisée!'
    end
  end

end
