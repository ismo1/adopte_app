class Relationship < ActiveRecord::Base
  attr_accessible :followed_id

  # par défaut rails cherchera la clé etrangère dans la class de correspondance
  # selon ses convention de nommage mais dans ce cas y'a pas de models Follower
  # ni de model Followed alors il faudras lui préciser que ça fait référence à la classe (model) User.
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"

  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
# == Schema Information
#
# Table name: relationships
#
#  id          :integer         not null, primary key
#  follower_id :integer
#  followed_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

