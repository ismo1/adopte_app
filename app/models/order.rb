class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy

  attr_accessor :email

  attr_accessible :name, :last_name, :address, :city, :code_postal, :email, :email_confirmation

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  before_save { |order| order.email = email.downcase }
  # "Check", "Credit card", "Purchase order"

  PAYMENT_TYPES = ["Bon de commande", "Carte de Crédit","Chèque"]
  attr_accessible :address, :email, :name, :pay_type
  has_many :line_items, dependent: :destroy
  # ...
  validates :name, :last_name, :address,:city, presence: true

  validates :code_postal, presence: true,
            :format =>{:with => /\A[0-9]{3,5}\z/, :message => "format incorrecte"}

  validates :email, presence: true,
            :format => { :with => email_regex, message: ": format incorrecte" }

  validates_confirmation_of :email, :message => "doit ressembler l'e-mail saisie"

  validates :pay_type, inclusion: PAYMENT_TYPES, presence: {:is => true, :message => "vous devez choisir un moyen de Payement"}

  def add_line_items_from_cart(cart) cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def self.current_user_orders(user_id)

    Product.where("user_id = ?", $user_id).each do |pp|
      @current_user_products_id << pp.id
    end

    # Product.where("user_id = ?", 3).each do |pp|
    # p_id << pp.id
    # end

    # Order.last.line_items.where(:product_id => @current_user_products_id)

    Order.find_each do |order|
      order.line_items.where(:product_id => @current_user_products_id).each  do |product|
        @current_user_orders << order
      end
    end
    return @current_user_orders
  end
end

# == Schema Information
#
# Table name: orders
#
#  id          :integer         not null, primary key
#  name        :string(255)
#  address     :text
#  email       :string(255)
#  pay_type    :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  city        :string(255)
#  last_name   :string(255)
#  code_postal :string(255)
#

