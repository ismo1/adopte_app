class DataFile < ActiveRecord::Base
  def self.save(upload)
file = DataFile.new

    if upload != ""

      file_name = File.basename(upload['datafile'].original_filename)
      file_extention = File.extname(name)
      directory = "app/assets/images/store/"

      file_new_name =   Time.now.strftime("%Y_%m_%d_%H_%M_%S_") + file_name
      # create the file path
      path = File.join(directory, file_new_name)

      Dir.mkdir(directory) unless File.directory?(directory)

      # write the file
      File.open(path, "wb") { |f| f.write(upload['datafile'].read) }

    file.name   = file_new_name
   file.file_type   = upload['datafile'].content_type.chomp
    file.path   = path.gsub(/public/, '')
    
    file.save
    return file
    end

  end

  def sanitize_filename(file_name)
    # get only the filename, not the whole path (from IE)
    just_filename = File.basename(file_name)
    # replace all none alphanumeric, underscore or perioids
    # with underscore
    just_filename.sub(/[^\w\.\-]/,'_')
  end

  def cleanup(dirname)
    File.delete("#{RAILS_ROOT}/#{dirname}/#{@filename}") if File.exist?("#{RAILS_ROOT}/#{dirname}/#{@filename}")
  end

end

# == Schema Information
#
# Table name: data_files
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  path       :string(255)
#  file_type  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

