class Category < ActiveRecord::Base
  has_many :product, :autosave => true
  attr_accessible :id, :description, :name
end
# == Schema Information
#
# Table name: categories
#
#  id          :integer         not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

