class Product < ActiveRecord::Base

  has_many :line_items
  has_many :orders, through: :line_items
  validates_associated :category

  before_destroy :ensure_not_referenced_by_any_line_item

  attr_accessible :description, :image_url, :price, :title, :category_id, :user_id, :status
  validates :title, :description, :image_url, presence: true
  validates :price, numericality: {greater_than_or_equal_to: 0.01}
  #
  validates :title, uniqueness: true
  validates :image_url, allow_blank: true, format: {
    with:    %r{\.(gif|jpg|png)\Z}i,
    message: 'must be a URL for GIF, JPG or PNG image.'
  }
  validates :title, length: {minimum: 5}


 
 
  CATEGORIES = Category.find(:all).collect {|category| category.id}

  belongs_to :category
  validates :category_id, inclusion: CATEGORIES
  # def category
  # Category.find_by_id(category_id).name
  # end

  PRODUCT_STATUS = ["Disponible","Indisponible", "Déstockage", "Solde", "Promotion"]
  validates :status, inclusion: PRODUCT_STATUS, presence: {:is => true, :message => "vous devez indiquer le statut du produit"}
  
  belongs_to :user
  validates :user_id, presence: true
  def user
    User.find_by_id(user_id)
  end
    
  def status_class
    
    case status # a_variable is the variable we want to compare
    when "Disponible"  #compare to 1
    @status =  "success"
    when "Indisponible" #compare to 2
    @status =  "warning" 
    when "Déstockage" #compare to 2
    @status =  "important" 
    when "Solde" #compare to 2
    @status =  "info" 
    when "Promotion" #compare to 2
    @status =  "info" 
    else
    @status =  "inverse" 
    end

  end
  
  
  
  #********************* Search ********************
  def self.search(search)
    if search
      find(:all, :conditions => [ "lower(title) LIKE ? OR lower(status) LIKE ? OR lower(status) LIKE ?", "%#{search}%".downcase, "%#{search}%".downcase, "%#{search}%".downcase ], :order => 'title' )
    else
      find(:all)
    end
  end

  

  private

  # ensure that there are no line items referencing this product
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
    return true
    else
      errors.add(:base, 'Line Items present')
    return false
    end
  end
end# == Schema Information

#
# Table name: products
#
#  id          :integer         not null, primary key
#  title       :string(255)
#  description :text
#  image_url   :string(255)
#  price       :decimal(8, 2)
#  created_at  :datetime
#  updated_at  :datetime
#

# == Schema Information
#
# Table name: products
#
#  id          :integer         not null, primary key
#  title       :string(255)
#  description :text
#  image_url   :string(255)
#  price       :decimal(8, 2)
#  created_at  :datetime
#  updated_at  :datetime
#  category_id :integer
#  user_id     :integer
#  status      :string(255)
#

# == Schema Information
#
# Table name: products
#
#  id          :integer         not null, primary key
#  title       :string(255)
#  description :text
#  image_url   :string(255)
#  price       :decimal(8, 2)
#  created_at  :datetime
#  updated_at  :datetime
#  category_id :integer
#  user_id     :integer
#  status      :string(255)
#

# == Schema Information
#
# Table name: products
#
#  id          :integer         not null, primary key
#  title       :string(255)
#  description :text
#  image_url   :string(255)
#  price       :decimal(8, 2)
#  created_at  :datetime
#  updated_at  :datetime
#  category_id :integer
#  user_id     :integer
#  status      :string(255)
#

