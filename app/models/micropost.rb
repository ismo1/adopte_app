class Micropost < ActiveRecord::Base
  attr_accessible :content

  belongs_to :user

  post_max_size = 140
  validates :content, presence: { :is => true,  message: ": ne peut pas être vide" },
                      length: { maximum: 140, too_long: ": la longeur maximale autorisée est #{post_max_size}", :post_max_size => post_max_size }

  # In the case of the Micropost model, there is only one attribute
  # that needs to be editable through the web, namely,
  # the content attribute, so :user_id no be set in
  # the accessible list
  validates :user_id, presence: true

  #here is a rails facilitator and :order paramters
  #which permit to sort the microposts (Default scope)
  default_scope order: 'microposts.created_at DESC'
  
  
  # def self.from_users_followed_by(user)
    # #[1, 2, 3, 4].map(&:to_s).join(', ') or
    # #user.followed_users.map(&:id) or 
    # #user.followed_user_ids.join(', ') or
    # followed_user_ids = user.followed_user_ids
    # where("user_id IN (?) OR user_id = ?", followed_user_ids, user)
  # end
  
  # Returns microposts from the users being followed by the given user.
  def self.from_users_followed_by(user)
    
 # SELECT * FROM microposts
      # WHERE user_id IN (SELECT followed_id FROM relationships
      # WHERE follower_id = 1)
      # OR user_id = 1
    
    followed_user_ids = "SELECT followed_id FROM relationships
                         WHERE follower_id = :user_id"
    where("user_id IN (#{followed_user_ids}) OR user_id = :user_id",
          user_id: user.id)
  end
end
# == Schema Information
#
# Table name: microposts
#
#  id         :integer         not null, primary key
#  content    :string(255)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

