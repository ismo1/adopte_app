# == Schema Information
#
# Table name: users
#
#  id                 :integer         not null, primary key
#  nom                :string(255)
#  email              :string(255)
#  salt               :string(255)
#  encrypted_password :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  remember_token     :string(255)
#
class User < ActiveRecord::Base

  #crée un attribut password virtuel qui n'existe pas dans le bdd

  attr_accessor :password, :admin

  attr_accessible :nom, :email, :password, :password_confirmation

  # Ensuring that a user’s microposts are destroyed along with the user.
  has_many :microposts, dependent: :destroy
  
   has_many :products, dependent: :destroy

  #****************************** Follower/Followed********************
  #ici on précise à rails de concidérer follower_id de latable :relationships comme la clé étrangère appartenant à user
  has_many :relationships, foreign_key: "follower_id", dependent: :destroy

  has_many :followed_users, through: :relationships, source: :followed

  has_many :reverse_relationships, foreign_key: "followed_id",
                                   class_name:  "Relationship",
                                   dependent:   :destroy
  has_many :followers, through: :reverse_relationships, source: :follower

  # the relationships association uses the follower_id foreign key
  # and the reverse_relationships association uses followed_id

  before_save :create_remember_token
  #has_secure_password

  before_save { |user| user.email = email.downcase }
  #appel cette fonction pour générer un mot de passe crypté avant le sauvegarde
  before_save :encrypt_password
  
  before_save { generate_token(:auth_token) }

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  name_max_size = 50
  validates :nom, :presence => { :is => true,  message: ": ne peut pas être vide" },
                  :length => {:maximum => name_max_size, message: ": la longeur maximale autorisée est #{name_max_size}"}

  validates :email, :presence => { :is => true,  message: ": ne peut pas être vide" },
                    :format => { :with => email_regex, message: ": format incorrecte" },
                    :uniqueness => {:case_sensitive => false, message: ": Existe déjà dans la base de données"}

  password_min_size = 6
  password_max_size = 30
  # Crée automatiquement l'attribut virtuel 'password_confirmation'.

   validates :password, :presence => { :is => true,  message: ": ne peut pas être vide"},
  :confirmation => { :is => true,  message: ": doit correspondre au Mot de Passe" },
  length: { within: password_min_size..password_max_size,
  too_short: ": la longeur minimale autorisée est #{password_min_size}",
  too_long: ": la longeur maximale autorisée est #{password_max_size}"}   
 #validates :password, :presence =>true, :confirmation => true, :length => { :within => password_min_size..password_max_size }, :on => :update_profile  
 # alidates :password, :confirmation => true, :length => { :within => 6..30 }, :on => :create, :unless => lambda{ |user| user.password.blank? } 
#   

  # Pour vérifier qu'un mot de passe soumis coïncide avec le mot de passe
  # de l'utilisateur, nous cryptons d'abord la chaine de caractères soumise
  # puis nous la comparons au mot de passe crypté enregistré.

  # Retour true (vrai) si le mot de passe correspond.
  def has_password?(password_soumis)
    # Compare encrypted_password avec la version cryptée de password_soumis.
    @password_string = password_soumis
    encrypt_password == encrypt_password_generator
  end

  #self utilisé dans la signature d'une méthode fait de la méthode
  #une méthode classe, car il fait référence à la classe elle même
  #self.methodeName == ClassName.methodeName
  def self.authenticate(email, submit_password)
    user = User.find_by_email(email.downcase)
    #return user
    return nil if user.nil?
    return user if user.has_password?(submit_password)
  end
  
  #********************** Micropost *******************
  #récupère tous les posts de l'utilisateurs courant
  def feed
    #where("user_id in (?) OR user_id = ?", following_ids, user)
    #in sql :
    # SELECT * FROM microposts
    # WHERE user_id IN (<list of ids>) OR user_id = <user id>
    Micropost.from_users_followed_by(self)

  #feed for all current_user's followed
  #feed for current user only : we can use this: microposts  or : Micropost.where("user_id = ?", id) the two are equal
  end
  
  def feed_product
    
    Product.where(:user_id => id)

  end
  
  

  #********************* Search ********************
  def self.search(search)
    if search
      find(:all, :conditions => [ "lower(nom) LIKE ?", "%#{search}%".downcase ])
    else
      find(:all)
    end
  end

  #********************* Follower/Followed*******************************
  #self.relationships ..... est equivalent à relationships ....

  def following?(other_user)
    relationships.find_by_followed_id(other_user.id)
  end

  #create a line of relationship beetwen curent user and other_user
  def follow!(other_user)
    relationships.create!(followed_id: other_user.id)
  end

  def unfollow!(other_user)
    relationships.find_by_followed_id(other_user.id).destroy
  end

  #unfollow! is straightforward: just find the relationship by followed id and destroy

#***************** Password reset ***********************
def generate_token(column)
  begin
    self[column] = SecureRandom.urlsafe_base64
  end while User.exists?(column => self[column])
end

def send_password_reset
  generate_token(:password_reset_token)
  self.password_reset_sent_at = Time.zone.now
  save
  UserMailer.password_reset(self).deliver
end


def set_as_admin
  self.admin = true
  save
end

def unset_as_admin
  self.admin = false
  save
end

  private

  #self utilisé à l'intérieur d'une méthode
  # fait référence à un objet Instance de la classe.
  def encrypt_password
    self.salt = make_salt if new_record?
    self.encrypted_password = encrypt_password_generator #encrypt(password)
  end

  #ici salt tout court c'est l'équivalent
  # de self.salt qui est égale aussi à l'instance user.salt
  def encrypt_password_generator
    if new_record?
      pass = password
    else
    pass = @password_string
    end
    #ajout de grain de sable supplémentaire $??$
    secure_hash("#{salt}-$??$-#{pass}")
  end

  # def encrypt(password_string)
  # #ajout de grain de sable supplémentaire $??$
  # secure_hash("#{salt}-$??$-#{password_string}")
  # end

  def make_salt
    secure_hash("#{Time.now.utc}-$?€€?$-#{password}")
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end

  #******************* Remember Token ********************
  def create_remember_token
    # Create the token. and assign its value to remember_token'field of the current user instance
    self.remember_token = SecureRandom.urlsafe_base64
  end

end
# == Schema Information
#
# Table name: users
#
#  id                     :integer         not null, primary key
#  nom                    :string(255)
#  email                  :string(255)
#  salt                   :string(255)
#  encrypted_password     :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  remember_token         :string(255)
#  admin                  :boolean         default(FALSE)
#  auth_token             :string(255)
#  password_reset_token   :string(255)
#  password_reset_sent_at :datetime
#

