xml.instruct!
xml.posts do
  @posts.each do |post|
    xml.post do
      xml.title post.title
      xml.body post.body
      xml.published_at post.published_at
      xml.comments do
        post.comments.each do |comment|
          xml.comment do
            xml.body comment.body
          end
        end
      end
    end
  end
end

xml.instruct!
xml.orders do
  @product.orders.each do |order|
    xml.order do
      xml.title "Order #{order.id}"
      xml.body post.body
      xml.published_at post.published_at
      xml.comments do
        post.comments.each do |comment|
          xml.comment do
            xml.body comment.body
          end
        end
      end
    end
  end
end








xml.orders do
@product.orders.each do |order|


<orders type="array">
<order>
<id type="integer">1</id>
<name>Moussa Seybou</name>
<address>10 chemin des Bourgeois Aulnoy Lez Valenciennes</address>
<email nil="true"/>
<pay-type>Carte de Crédit</pay-type>
<created-at type="dateTime">2013-06-17T23:27:27Z</created-at>
<updated-at type="dateTime">2013-06-17T23:27:27Z</updated-at>
<city nil="true"/>
<last-name nil="true"/>
<code-postal nil="true"/>
</order>
</orders>