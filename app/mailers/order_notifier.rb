class OrderNotifier < ActionMailer::Base
  default from:  "myShop <moussa.ismo1@gmail.com>"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.received.subject
  #
  def received(order)
    @greeting = "Bonjour !"
    @order = order
    #attachments.inline['photo.png'] = File.read('path/to/photo.png')
    #path = "app/assets/images/store/"
    path = "#{Rails.root}/app/assets/images/"
    @order.line_items.each do |item|
      #fichier affcihé directement dans le mail
      attachments.inline["#{item.product.image_url}"] = File.read("#{path}#{item.product.image_url}")
      
      #Pièce Jointes
      #attachments["#{item.product.image_url}"] = File.read("#{path}#{item.product.image_url}")
    end
    # attachments.inline['ruby'] = File.read("#{RAILS_ROOT}/app/assets/images/ruby.png")
    # attachments.inline['ruby2'] = File.read("#{RAILS_ROOT}/app/assets/images/ruby.png")
    mail to: order.email, subject: 'Boutique myShop | Confirmation de Commande'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.shipped.subject
  #
  def shipped(order)
    @order = order
    mail to: order.email, subject: 'Boutique myShop | Envoie de votre commande'
  end
end