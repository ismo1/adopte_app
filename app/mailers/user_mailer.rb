class UserMailer < ActionMailer::Base
  default from: "myShop <moussa.ismo1@gmail.com>"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @greeting = "Hi"
    @user = user
    mail :to => user.email, :subject => "Password Reset"
  end

end