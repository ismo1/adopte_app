module ApplicationHelper
  # ajoute le logo dans l'application
  def logo
    image_tag( 'logo_logement.png', :alt => "adopte1Coloc", :class => "smallone")
  end

  #retourner un titre de basé sur la page
  def titre
    base_titre = "myShop Web-App"
    if @titre.nil?
    base_titre
    else
      "#{base_titre} | #{@titre}"
    end
  end

#content_tag() can be used to wrap the output created by a block in a tag.
#By using the &block notation, we get Ruby to pass the block 
#that was given to hidden_div_if() down to content_tag().
  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display: none"
    end
    content_tag("div", attributes, &block)
  end
end