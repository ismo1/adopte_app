module UsersHelper
  def signup
    @titre = "Inscription"
    @serach_comp =  display_search_component?
  end

  # def gravatar_for(user, options = { :size => 50 })
  # gravatar_image_tag(user.email.downcase, :alt => user.nom,
  # :class => 'gravatar',
  # :gravatar => options)
  # end

  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, options = { :size => 55 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    #image_tag(gravatar_url, alt: user.nom, class: "gravatar", gravatar: options)

    gravatar_image_tag(gravatar_url, :alt => user.nom,
                                            :class => 'gravatar',
                                            :gravatar => options)

  end

end
