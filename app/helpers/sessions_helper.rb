module SessionsHelper
  def sign_in(user)
    # Under the hood, using permanent causes Rails to set the expiration
    # to 20.years.from_now automatically.
    cookies.permanent[:remember_token] = user.remember_token

    #The purpose of this line is to create current_user, accessible in both controllers and views,
    #which will allow constructions such as
    #<%= current_user.name %>
    #and
    #redirect_to current_user

    #The use of self is necessary in this context because
    #without self, Ruby would simply create a local variable called current_user.
    self.current_user = user
  end

  #  self.current_user = user is an assignment, which we must define.
  #Ruby has a special syntax for defining such an assignment function
  def current_user=(user)
    @current_user = user
  end

  # $ rails console
  # >> x = 1
  # => 1
  # >> x += 1
  # => 2
  # >> x *= 3
  # => 6
  # >> x -= 7
  # => -1
  #
  # >> @user
  # => nil
  # >> @user = @user || "the user"
  # => "the user"
  # >> @user = @user || "another user"
  # => "the user"
  #
  # >> @user ||= "the user"
  # => "the user"

  # calls the find_by_remember_token method the first time current_user is called, but on subsequent
  # invocations returns @current_user without hitting the database.7 This is only useful
  # if current_user is used more than once for a single user request; in any case,
  # find_by_remember_token will be called at least once every time a user visits a page on the site.
  def current_user
    @current_user ||= User.find_by_remember_token(cookies[:remember_token])
    @current_user ||= User.find_by_auth_token(cookies[:auth_token]) if cookies[:auth_token]
  end

  def current_user?(user)
    user == current_user
  end

  # A user is signed in if there is a current user in the session, i.e., if current_user is non-nil.
  # This requires the use of the “not” operator, written using an exclamation point ! and usually read as “bang”.
  #present context, a user is signed in if current_user is not nil,
  def signed_in?
    !self.current_user.nil?
  end

  def signed_in_user
    unless signed_in?
      redirect_to signin_url, notice: "#{t :sign_in_message}"
      store_location #on sauvegarde la requete de départ ayant conduit à la page d'authentification
    end
  end

  def sign_out
    self.current_user = nil
    cookies.delete(:remember_token)
    cookies.delete(:auth_token)
  end

  # Our site authorization is complete as written, but there is one minor blemish: when users try to access a protected page,
  # they are currently redirected to their profile pages regardless of where they were trying to go. In other words,
  # if a non-logged-in user tries to visit the edit page, after signing in the user
  # will be redirected to /users/1 instead of /users/1/edit. It would be much friendlier to redirect them to their
  # intended destination instead.

  # In order to forward users to their intended destination, we need to store the location of the requested page somewhere,
  # and then redirect to that location instead. We accomplish this with a pair of methods, store_location and redirect_back_or,
  # both defined in the Sessions helper

  # The storage mechanism is the session facility provided by Rails, which you can think of as being like an instance of the cookies
  # variable from Section 8.2.1 that automatically expires upon browser close. We also use the request object to get the url, i.e.,
  # the URI/URL of the requested page. The store_location method puts the requested URI in the session variable under the key :return_to.
  # To make use of store_location, we need to add it to the signed_in_user before filter

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    session.delete(:return_to)
  end

  def store_location
    session[:return_to] = request.url
  end

  #this methode must be call always before calling  redirect_back_or(default)
  def stored_location?
    !session[:return_to].nil?
  end

  #********************** Admin **************************

  def authorize
    signed_in_user
  end

  #******************* Store *****************************
  private

  def current_cart
    Cart.find(session[:cart_id])
  rescue ActiveRecord::RecordNotFound
    cart = Cart.create
    session[:cart_id] = cart.id
    cart
    end

end
