// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery.ui.all
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require bootstrap-datepicker
//= require_tree .

$("#menu_id > li > a").mouseover(function() {
	$(this).parent().addClass('active')
});

$("#menu_id > li > a").mouseleave(function() {
	$(this).parent().removeClass('active')
});

$("#menu_id > li > a").click(function() {
	$(this).parent().addClass('active')
});	


$(document).ready(function() { 
 
  $('#btn_reset_password').click(function() {  
 
    $(".error").hide();
    var hasError = false;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var emailblockReg =
     /^([\w-\.]+@(?!gmail.com)(?!yahoo.com)(?!hotmail.com)([\w-]+\.)+[\w-]{2,4})?$/;
 
    var emailaddressVal = $("#email").val();
    if(emailaddressVal == '') {
      $("#email").after('<span class="error">Please enter your email address.</span>');
      hasError = true;
    }
 
    else if(!emailReg.test(emailaddressVal)) {
      $("#email").after('<span class="error">Enter a valid email address.</span>');
      hasError = true;
    }
 
    else if(!emailblockReg.test(emailaddressVal)) {
      $("#email").after('<span class="error">No yahoo, gmail or hotmail emails.</span>');
      hasError = true
    } 
 
    if(hasError == true) { return false; }
 
    });
});
