# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
AdopteApp::Application.config.secret_key_base = 'f1c95e1f36e469ae8daa96d3b71e35526b2d0167fd39a45e92a2a0a002b183923b5791ad39d7b27fa5f8cf9f3e6260c633db78f51e64a589c7ffc0c563e2b73a'
