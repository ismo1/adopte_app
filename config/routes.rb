AdopteApp::Application.routes.draw do

#when you call the books_path method you should get "/en/books"
#(for the default locale).
# scope "/:locale" do
# resources :users
# end

# scope "(:locale)", :locale => /en|fr/ do
  scope "(:locale)" do
  # resources :users
    resources :users do
      member do
        get :following, :followers
        get :admin_edit
        get :edit_profile
        post :update_profile
      end
    end
    
    # post '/admin_edit', :to => 'data_file#uploadFile'

    resources :password_resets
    resources :sessions, :only => [:new, :create, :destroy]
    resources :microposts, only: [:index, :create, :destroy]
    resources :relationships, only: [:create, :destroy]

    #STore
    resources :products
    resources :line_items
    resources :carts
    resources :orders
    resources :categories

    resources :line_items do
      put 'decrement', on: :member
    end

    resources :products do
      get :who_bought, on: :member
    end

    resources :orders do
      collection do
        post '/by_date', :to => :orders_by_date
      end
    end


    get '/test1', :to => 'test_js#test1'
    get '/test2', :to => 'test_js#test2'

    get "/users/new"
    get '/contact', :to => 'pages#contact'
    get '/about', :to =>  'pages#about'
    get '/help', :to => 'pages#help'
    get '/site', :to =>  'pages#index'
    get '/home', :to => 'pages#home'
    get '/search', :to => 'pages#search'
    get '/autocomplete', :to => 'users#autocomplete'
    get '/signup', :to => 'users#new'

    get '/data_file', :to => 'data_file#index'
    post '/uploadFile', :to => 'data_file#uploadFile'
    get '/file', :to => 'data_file#show'

    get "password_resets/new"
    get "relationships/create"
    get "relationships/destroy"
    
    get '/search', :to => 'searches#show'

    controller :sessions do
      get 'signin' => :new
      post 'signin' => :create
      delete 'signout' => :destroy
    end

    get '/store', :to => 'store#index', as: 'store'

    get '/admin', :to => 'admin#index'

    root :to => 'store#index'

  end

# get '/signin',  :to => 'sessions#new'
# get '/signout', :to => 'sessions#destroy', :via => :delete #, via: :delete

# The priority is based upon order of creation: first created -> highest priority.
# See how all your routes lay out with "rake routes".

# You can have the root of your site routed with "root"
# root 'welcome#index'

#  root :to => 'pages#site'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end
get ':not_found' => redirect('/'), :constraints => { :not_found => /.*/ }
end
