class CreateUsers < ActiveRecord::Migration
  # def change
  # create_table :users do |t|
  # t.string :nom
  # t.string :email
  #
  # t.timestamps
  # end
  # end
  def self.up
    create_table :users do |t|
      t.string   :nom
      t.string   :email
      t.string   :salt
      t.string   :encrypted_password

      t.timestamps
    end
    add_index :users, :email, :unique => true
  end

  def self.down
    remove_index :users, :email
    drop_table :users
  end
end
