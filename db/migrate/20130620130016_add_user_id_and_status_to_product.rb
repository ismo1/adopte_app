class AddUserIdAndStatusToProduct < ActiveRecord::Migration
  def change
    add_column :products, :user_id, :integer
    add_column :products, :status, :string
    add_index :products, :user_id
  end
end
