class AddFieldsToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :city, :string
    add_column :orders, :last_name, :string
    add_column :orders, :code_postal, :string
  end
end
